\prompt 'Enter table name: ' table_name
SET val.name = :'table_name';
DO
$$DECLARE
 mytableName regclass;
 mycolumnNo INTEGER;
 mycolumnName VARCHAR;
 mydataType VARCHAR;
 mycomment VARCHAR;
 c RECORD;
 I RECORD;
 myIndexName VARCHAR;
BEGIN
 SELECT current_setting('val.name') INTO mytableName;
 FOR c IN (
 SELECT attnum, attname, format_type(atttypid, atttypmod), col_description(attrelid, attnum)
 FROM pg_attribute
 WHERE attrelid = mytableName::regclass AND attnum > 0 AND NOT attisdropped
 ORDER BY attnum
 )
 LOOP
 mycolumnNo := c.attnum;
 mycolumnName := c.attname;
 mydataType := c.format_type;
 mycomment := c.col_description;
 myIndexName := NULL;

 FOR i IN (
 SELECT indexname
 FROM pg_indexes
 WHERE tablename = mytableName::text AND indexdef LIKE '%'  || mycolumnName ||  '%'
 )
 LOOP
 myIndexName := i.indexname;
 END LOOP;
 
 RAISE NOTICE '% % Type : % Comment : ''%'' Index : ''%''', mycolumnNo, mycolumnName, mydataType, mycomment, myIndexName;
 END LOOP;
END $$;